<?php
/**
* Plugin Name: Simple Flake
* Plugin URI: http://patrickblackjr.com
* Description:  Super simple plugin to simply add snowflakes to your WordPress installation.
* Version: 1.0.0
* Author: Mortiferr
* Author URI: patrickblackjr.com
* License: GPL12
*/
// If this file is called directly, then the file should die.
if ( ! defined( 'WPINC' ) ) {
	die;
}
require('core.js');