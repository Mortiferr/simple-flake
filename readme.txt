=== Simple Flakes ===
Contributors: Mortiferr
Tags: snowflakes, snow flakes, flakes, snow, holiday, christmas, winter
Requires at least: 4.0
Tested up to: 4.5 beta releases
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Super simple plugin to simply add snowflakes to your WordPress installation.

== Description ==
Super simple plugin to simply add snowflakes to your WordPress installation. No config, no hassle.

== Installation ==
Flake is simple install.
Upload the simple-flakes.zip in your plugin area. Activate. Done.
-
Upload the /simple-flakes/ folder to your /wp-content/plugins/ directory.
Activate the plugin through the Plugins menu in WordPress.
Reload your page. 

== Frequently Asked Questions ==
None yet!

== Changelog ==
=Version 1.0=
Super simple plugin to simply add snowflakes to your WordPress installation. No config, no hassle.

== Upgrade Notice ==
Please upgrade as soon as possible to patch a jQuery update.